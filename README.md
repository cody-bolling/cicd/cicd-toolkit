<!-- PROJECT SHIELDS -->
[![Latest Release](https://gitlab.com/cody-bolling/cicd/cicd-toolkit/-/badges/release.svg)](https://gitlab.com/cody-bolling/cicd/cicd-toolkit/-/releases) [![pipeline status](https://gitlab.com/cody-bolling/cicd/cicd-toolkit/badges/main/pipeline.svg)](https://gitlab.com/cody-bolling/cicd/cicd-toolkit/-/commits/main)

<!-- PROJECT LOGO -->
<br/>
<div align="center">
  <a href="https://gitlab.com/cody-bolling/cicd/cicd-toolkit">
    <img src="https://gitlab.com/cody-bolling/project-boilerplate-generator/-/raw/main/logos/cicd-toolkit.png" alt="Logo" width="100" height="100">
  </a>
  <h3 align="center">
    CICD Toolkit
  </h3>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary><b>Table of Contents</b></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#image-information">Image Information</a></li>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#license">License</a></li>
  </ol>
</details>

## About The Project

A repository that manages the **CICD Toolkit** image. The image is intended to be used in GitLab CICD pipelines.

### Image Information

```text
public.ecr.aws/cody-bolling/cicd-toolkit:latest
```

[AWS ECR Public listing](https://gallery.ecr.aws/b9b5m9f7/cicd-toolkit)

This image uses [Alpine](https://alpinelinux.org/) 3.15 for its base Linux distribution. Below is a list of installed packages.

- aws-cli v2
- curl
- git
- gitlab release-cli
- glibc
- kaniko executor

### Built With

- [AWS ECR](https://aws.amazon.com/ecr/)
- [GitLab CICD](https://docs.gitlab.com/ee/ci/)

## Roadmap

See the [open issues](https://gitlab.com/cody-bolling/cicd/cicd-toolkit/-/boards) for a full list of proposed features and known issues.

## License

Distributed under the MIT License. See `LICENSE.txt` for more information.
